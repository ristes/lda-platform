#!/bin/bash

for i in `ls policies`
do
    cat policies/$i | sed -e 's/ALLOW/SELECT/' -e 's/DENY/SELECT/' -e 's/ READ//' -e 's/ INSERT//' -e 's/ DELETE//' -e 's/MANAGE/*/' -e 's/MODIFY//' | awk -f  remove-intent-binding.awk > queries/$i
done
