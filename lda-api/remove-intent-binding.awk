BEGIN {
    inAlpha=0;
    open=0;
}
/GRAPH <http:\/\/intent>/ {
    inAlpha=1;
}
/\{/ {
    if(inAlpha==1) {
        open++;
    }
}
{
    if(inAlpha==0) {
        print $0;
    }
}
/\}/ {
    if(inAlpha==1) {
        open--;
    }
    if(open==0) {
        inAlpha=0;
    }
}
