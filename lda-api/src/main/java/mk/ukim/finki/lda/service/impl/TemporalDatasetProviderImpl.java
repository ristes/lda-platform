package mk.ukim.finki.lda.service.impl;

import mk.ukim.finki.lda.model.Operation;
import mk.ukim.finki.lda.service.PolicyProvider;
import mk.ukim.finki.lda.service.PolicyTransformationService;
import mk.ukim.finki.lda.service.TemporalDatasetProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Riste Stojanov
 */
@Service
public class TemporalDatasetProviderImpl implements TemporalDatasetProvider {

  private PolicyTransformationService policyTransformationService;

  private PolicyProvider policyProvider;

  @Autowired
  public TemporalDatasetProviderImpl(
    PolicyTransformationService policyTransformationService,
    PolicyProvider policyProvider) {
    this.policyTransformationService = policyTransformationService;
    this.policyProvider = policyProvider;
  }

  @Override
  public String temporalDatasetQuery(Operation operation) {
    return policyTransformationService.allowedDataQuery(policyProvider.policies(), operation);
  }
}
