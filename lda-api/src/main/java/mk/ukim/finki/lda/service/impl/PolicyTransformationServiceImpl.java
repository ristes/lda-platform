package mk.ukim.finki.lda.service.impl;

import mk.ukim.finki.lda.model.Operation;
import mk.ukim.finki.lda.model.Policy;
import mk.ukim.finki.lda.service.PolicyTransformationService;
import mk.ukim.finki.lda.utils.ElementUtils;
import mk.ukim.finki.lda.web.dto.ConflictInfo;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.sparql.syntax.Element;
import org.apache.jena.sparql.syntax.ElementGroup;
import org.apache.jena.sparql.syntax.ElementMinus;
import org.apache.jena.sparql.syntax.ElementUnion;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Riste Stojanov
 */
@Service
public class PolicyTransformationServiceImpl implements PolicyTransformationService {

  @Override
  public String coverageQuery(List<Policy> policies, String prefixes) {

    ElementUnion union = new ElementUnion();

    policies.forEach(p -> union.addElement(p.protectedData));

    String sparqlQuery = prefixes + "SELECT ?s ?p ?o ?g \nWHERE { \n" + union.toString() + " }";

    System.out.println(sparqlQuery);
    return sparqlQuery;
  }

  @Override
  public String unprotectedQuery(List<Policy> policies, String prefixes) {
    String everythingElementString = "{ { GRAPH ?g {?s ?p ?o} } UNION { ?s ?p ?o} }";
    Element everithingElement = QueryFactory.createElement(everythingElementString);

    ElementUnion union = new ElementUnion();

    policies.forEach(p -> union.addElement(p.protectedData));

    ElementMinus minus = new ElementMinus(union);

    ElementGroup group = new ElementGroup();
    group.addElement(everithingElement);
    group.addElement(minus);

    String sparqlQuery = prefixes + "SELECT ?s ?p ?o ?g \nWHERE { \n" + group.toString() + " }";

    System.out.println(sparqlQuery);
    return sparqlQuery;
  }

  @Override
  public List<ConflictInfo> conflictInfoQueries(Policy policy, List<Policy> policies, String prefixes) {


    StringBuilder query = new StringBuilder(prefixes + "SELECT ?s ?p ?o ?g ");
    StringBuilder groupByMutable = new StringBuilder("GROUP BY  ?s ?p ?o ?g ");
    for (String var : policy.minimalIntentBindingVars) {
      query.append(var).append(" ");
      groupByMutable.append(var).append(" ");
    }
    final String groupByImmutable = groupByMutable.toString();
    final String immutableQuery = query.toString();

    List<ConflictInfo> results = new ArrayList<>();

    policies.stream()
      .filter(p -> p.operation != Operation.MANAGE)
      .filter(p -> !p.name.equals(policy.name))
      .filter(p -> p.permission != policy.permission)
      .forEach(p -> {
        StringBuilder conflictQuery = new StringBuilder(immutableQuery);
        StringBuilder groupBy = new StringBuilder(groupByImmutable);
        for (String var : p.minimalIntentBindingVars) {
          conflictQuery.append(var).append(" ");
          groupBy.append(var).append(" ");
        }

        conflictQuery.append(" WHERE {\n{\n");
        conflictQuery.append(policy.protectedData.toString());
        conflictQuery.append("} .\n{\n");
        conflictQuery.append(p.protectedData.toString());
        conflictQuery.append("}\n}\n");
        conflictQuery.append(groupBy);

        results.add(new ConflictInfo(policy, p, QueryFactory.create(conflictQuery.toString()).serialize()));
      });
    return results;
  }

  @Override
  public String allowedDataQuery(List<Policy> policies, Operation operation) {
    return "SELECT ?s ?p ?o ?g \nWHERE \n " + ElementUtils.allowedDataElement(
      policies
        .stream()
        .filter(policy -> policy.operation.matches(operation))
        .collect(Collectors.toList())
    ).toString();

  }
}
