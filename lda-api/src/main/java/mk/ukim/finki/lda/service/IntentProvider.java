package mk.ukim.finki.lda.service;

import org.apache.jena.rdf.model.Model;

/**
 * @author Riste Stojanov
 */
public interface IntentProvider<E> {

  Model toIntent(E evidences);
}
