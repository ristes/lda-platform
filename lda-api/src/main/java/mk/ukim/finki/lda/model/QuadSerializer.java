package mk.ukim.finki.lda.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.apache.jena.sparql.core.Quad;

import java.io.IOException;

/**
 * @author Riste Stojanov
 */
public class QuadSerializer extends JsonSerializer<Quad> {
  @Override
  public void serialize(Quad value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
    gen.writeString(value.toString());
  }
}
