package mk.ukim.finki.lda.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.apache.jena.sparql.syntax.Element;

import java.io.IOException;

/**
 * @author Riste Stojanov
 */
public class ElementConverter extends JsonSerializer<Element> {
  @Override
  public void serialize(Element value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
    gen.writeString(value.toString());
  }
}
