package mk.ukim.finki.lda.web;

import mk.ukim.finki.lda.IntentProvider;
import mk.ukim.finki.lda.model.Policy;
import mk.ukim.finki.lda.service.LdaService;
import mk.ukim.finki.lda.service.PolicyProvider;
import mk.ukim.finki.lda.web.dto.ConflictInfo;
import mk.ukim.finki.lda.web.dto.QueryResult;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.sparql.core.DatasetGraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

import static mk.ukim.finki.lda.utils.LoadUtils.parseDataGraph;

/**
 * @author Riste Stojanov
 */

@CrossOrigin({
  "http://localhost:4200",
  "http://lda.finki.ukim.mk"
})
@RestController
public class SecuredEndpoint {

  private LdaService ldaService;

  private IntentProvider<Pair<String, Lang>> intentProvider;

  private PolicyProvider policyProvider;

  @Autowired
  public SecuredEndpoint(LdaService ldaService, IntentProvider<Pair<String, Lang>> intentProvider, PolicyProvider policyProvider) {
    this.ldaService = ldaService;
    this.intentProvider = intentProvider;
    this.policyProvider = policyProvider;
  }


  @RequestMapping(method = RequestMethod.GET, value = "/insert")
  public QueryResult insert(@RequestParam String data, @RequestParam String intent) throws IOException {
    Model intentModel = intentProvider.extractIntent(new ImmutablePair<>(intent, RDFLanguages.JSONLD));
    DatasetGraph datasetGraph = parseDataGraph(data, RDFLanguages.TRIG);
    datasetGraph.find().forEachRemaining(System.out::println);
    return ldaService.insert(datasetGraph.find(), intentModel);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/delete")
  public QueryResult delete(@RequestParam String data, @RequestParam String intent) throws IOException {
    Model intentModel = intentProvider.extractIntent(new ImmutablePair<>(intent, RDFLanguages.JSONLD));
    DatasetGraph datasetGraph = parseDataGraph(data, RDFLanguages.TRIG);
    return ldaService.delete(datasetGraph.find(), intentModel);
  }


  @RequestMapping(method = RequestMethod.GET, value = "/policies")
  public List<Policy> policies() throws IOException {
    return policyProvider.policies();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/coverage")
  public QueryResult coverage() throws IOException {
    return ldaService.coverage();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/unprotected")
  public QueryResult unprotected() throws IOException {
    return ldaService.unprotected();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/conflict")
  public List<ConflictInfo> conflict(@RequestParam String name) throws IOException {
    return ldaService.detectConflicts(name);

  }


  @RequestMapping(method = RequestMethod.GET, value = "/sparql")
  public QueryResult sparql(@RequestParam(defaultValue = "SELECT * WHERE { GRAPH ?g {?s ?p ?o} }") String query, @RequestParam(required = false) String intent) throws IOException {

    Model intentModel = intentProvider.extractIntent(new ImmutablePair<>(intent, RDFLanguages.JSONLD));
    return ldaService.authorizedQuery(query, intentModel);

  }


  @RequestMapping(method = RequestMethod.GET, value = "/parse")
  public Policy parse(@RequestParam(defaultValue = "ALLOW READ { ?s ?p ?o ?g } WHERE { GRAPH ?g {?s ?p ?o} }") String query) throws IOException {
    return policyProvider.parse(query);
  }


  @RequestMapping(method = RequestMethod.GET, value = "/simulate")
  public QueryResult simulate(@RequestParam(defaultValue = "SELECT * WHERE { GRAPH ?g {?s ?p ?o} }") String query) throws IOException {
    return ldaService.executeWithoutAuthorization(query);
  }


}
