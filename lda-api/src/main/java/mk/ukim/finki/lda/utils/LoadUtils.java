package mk.ukim.finki.lda.utils;

import org.apache.jena.query.DatasetFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFParser;
import org.apache.jena.riot.system.ErrorHandlerFactory;
import org.apache.jena.sparql.core.DatasetGraph;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Riste Stojanov
 */
public class LoadUtils {


  public static Map<String, String> loadFiles(String dirPath) throws IOException {
    File dir = new File(dirPath);
    System.out.println(dir.getAbsolutePath());
    File[] files = dir.listFiles();
    Map<String, String> result = new HashMap<>();
    assert files != null;
    for (File q : files) {
      result.put(q.getName(), loadFile(q.getAbsolutePath()));
    }
    return result;
  }

  public static String loadFile(String prefixesPath) throws IOException {
    byte[] bytes = Files.readAllBytes(Paths.get(prefixesPath));
    return new String(bytes, Charset.forName("UTF-8"));
  }

  public static DatasetGraph parseDataGraph(String data, Lang lang) {
    DatasetGraph datasetGraph = DatasetFactory.create().asDatasetGraph();
    try (InputStream in = new ByteArrayInputStream(data.getBytes())) {
      parseDataGraph(datasetGraph, in, lang);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return datasetGraph;
  }


  public static DatasetGraph parseDataGraph(DatasetGraph datasetGraph, InputStream in, Lang lang) {
    RDFParser.create()
      .source(in)
      .lang(lang)
      .errorHandler(ErrorHandlerFactory.errorHandlerStrict)
      .parse(datasetGraph);
    return datasetGraph;
  }

  public static Map parsePrefixes(String prefixes, Map pm) {
    Arrays.stream(prefixes.split("\n")).forEach(prefixLine -> {
      String[] parts = prefixLine.split(" ");
      if (parts.length == 3) {
        pm.put(
          parts[1].substring(0, parts[1].length() - 1),
          parts[2].substring(1, parts[2].length() - 1)
        );
      }
    });
    return pm;

  }
}
