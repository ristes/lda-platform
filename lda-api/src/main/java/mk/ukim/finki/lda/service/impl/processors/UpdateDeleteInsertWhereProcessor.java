package mk.ukim.finki.lda.service.impl.processors;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.core.DatasetGraph;
import org.apache.jena.sparql.core.Quad;
import org.apache.jena.sparql.modify.request.UpdateDeleteInsert;
import org.apache.jena.sparql.syntax.Element;

import java.util.List;
import java.util.function.Consumer;

/**
 * @author Riste Stojanov
 */
public class UpdateDeleteInsertWhereProcessor extends AbstractUpdateProcessor<UpdateDeleteInsert, List<Quad>> {

  public void doProcess(
    UpdateDeleteInsert update,
    Model intent,
    Dataset dataset,
    String query,
    Consumer<Pair<String, List<Quad>>> consumer) {

    // Insert the quads
    List<Quad> deleteQuads = update.getDeleteQuads();
    List<Quad> insertQuads = update.getInsertQuads();
    Element where = update.getWherePattern();


    DatasetGraph dsg = dataset.asDatasetGraph();

    Query whereQuery = new Query();
    whereQuery.setQueryPattern(where);
    whereQuery.setQuerySelectType();
    whereQuery.setQueryResultStar(true);
    whereQuery.setResultVars();

    ResultSet rs = QueryExecutionFactory
      .create(whereQuery, getAllowedQuadsAsDataSet(dataset, query))
      .execSelect();


    throw new UnsupportedOperationException();

  }
}
