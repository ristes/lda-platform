package mk.ukim.finki.lda.tds;

import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;

/**
 * @author Riste Stojanov
 */
public class IntentGraphHandler {

  public static final String INTENT_IRI = "http://intent";
  private static long sequence = 0;

  public static synchronized String registerIntent(Dataset dataset, Model intent) {
    sequence++;
    String namedGraph = "http://intent/" + sequence;
    Node graph = NodeFactory.createURI(namedGraph);
    intent.getGraph().find().forEachRemaining(t -> dataset.asDatasetGraph().add(graph, t.getSubject(), t.getPredicate(), t.getObject()));
    return namedGraph;
  }
}
