package mk.ukim.finki.lda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Riste Stojanov
 */
@SpringBootApplication
public class LdaApplication {


  public static final String guardedDataset = "lda.guardedDataset";

  public static final String opQueriesPath = "lda.opQueriesPath";
  public static final String policiesPath = "lda.policiesPath";
  public static final String policyPrefixes = "lda.policyPrefixes";
  public static final String modify_commit = "lda.modify.commit";


  public static void main(String[] args) {
    SpringApplication.run(LdaApplication.class, args);
  }

}
