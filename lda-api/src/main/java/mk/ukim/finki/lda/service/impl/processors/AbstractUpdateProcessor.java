package mk.ukim.finki.lda.service.impl.processors;

import mk.ukim.finki.lda.model.Operation;
import mk.ukim.finki.lda.service.DatasetProvider;
import mk.ukim.finki.lda.service.PrologueProcessor;
import mk.ukim.finki.lda.service.TemporalDatasetProvider;
import mk.ukim.finki.lda.tds.IntentGraphHandler;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.core.DatasetGraph;
import org.apache.jena.sparql.core.Prologue;
import org.apache.jena.sparql.core.Quad;
import org.apache.jena.sparql.modify.request.UpdateDataDelete;
import org.apache.jena.sparql.modify.request.UpdateDataInsert;
import org.apache.jena.sparql.modify.request.UpdateDeleteWhere;
import org.apache.jena.update.Update;
import org.apache.jena.update.UpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author Riste Stojanov
 */
public abstract class AbstractUpdateProcessor<T extends Update, R> implements PrologueProcessor<R> {

  @Autowired
  protected DatasetProvider datasetProvider;

  @Autowired
  protected TemporalDatasetProvider temporalDatasetProvider;

  @Autowired
  protected Environment environment;

  public abstract void doProcess(T update,
                                 Model intent,
                                 Dataset dataset,
                                 String query,
                                 Consumer<Pair<String, R>> consumer);

  public void process(
    Prologue prologue,
    Model intent,
    Consumer<Pair<String, R>> consumer) {

    Dataset dataset = datasetProvider.guardedDataset();
    try {
      if (!(prologue instanceof UpdateRequest)) {
        throw new IllegalArgumentException();
      }

      List<Update> updates = ((UpdateRequest) prologue).getOperations();
      if (updates.size() != 1) {
        throw new IllegalArgumentException("Only one update operation is allowed per intent.");
      }


      T update = (T) updates.get(0);
      String query;
      if ((update instanceof UpdateDataDelete) || (update instanceof UpdateDeleteWhere)) {
        query = temporalDatasetProvider.temporalDatasetQuery(Operation.DELETE);
      } else if (update instanceof UpdateDataInsert) {
        query = temporalDatasetProvider.temporalDatasetQuery(Operation.INSERT);
      } else {
        throw new UnsupportedOperationException("Invalid query type: " + update);
      }


      dataset.begin(ReadWrite.WRITE);

      // Register Intent
      String namedGraph = IntentGraphHandler.registerIntent(dataset, intent);
      query = query.replace(IntentGraphHandler.INTENT_IRI, namedGraph);

      doProcess(update, intent, dataset, query, consumer);

      // clear the intent graph
      dataset.removeNamedModel(namedGraph);

      if (this.environment.getProperty("lda.modify.commit", Boolean.class, false)) {
        dataset.commit();
      } else
        dataset.abort();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      dataset.end();
    }
  }

  public void consumeAllowed(Dataset dataset,
                             String query,
                             Consumer<Quad> consumer) {

    // Filter out the allowed quads
    try (QueryExecution qexec = QueryExecutionFactory.create(query, dataset)) {
      ResultSet r = qexec.execSelect();
      r.forEachRemaining(qs -> {

        Quad quad;
        if (qs.get("?g") != null)
          quad = new Quad(
            qs.get("?g").asNode(),
            qs.get("?s").asNode(),
            qs.get("?p").asNode(),
            qs.get("?o").asNode());
        else
          quad = new Quad(
            Quad.unionGraph,
            qs.get("?s").asNode(),
            qs.get("?p").asNode(),
            qs.get("?o").asNode()
          );
        consumer.accept(quad);
      });
    }
  }


  public List<Quad> getAllowedQuadsAsList(Dataset dataset,
                                          String query) {

    final ArrayList<Quad> result = new ArrayList<>();
    // Filter out the allowed quads
    consumeAllowed(dataset, query, result::add);
    return result;
  }

  public Dataset getAllowedQuadsAsDataSet(Dataset dataset,
                                          String query) {

    Dataset result = datasetProvider.create();
    final DatasetGraph dsg = result.asDatasetGraph();
    // Filter out the allowed quads
    consumeAllowed(dataset, query, dsg::add);
    return result;
  }
}
