package mk.ukim.finki.lda.service.impl.sparql;

import mk.ukim.finki.lda.model.Policy;
import mk.ukim.finki.lda.tds.IntentGraphHandler;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.sparql.syntax.*;

import java.util.Set;

/**
 * @author Riste Stojanov
 */
public class PolicyExtractionElementVisitor implements ElementVisitor {

  private Policy policy;
  private boolean insideIntentBinding = false;

  public PolicyExtractionElementVisitor(Policy policy) {
    this.policy = policy;
  }

  @Override
  public void visit(ElementTriplesBlock el) {
    el.getPattern().getList().forEach(t -> {
      if (insideIntentBinding) {
        extractVars(t, policy.intentBindingVars, policy.protectedDataVars);
      } else {
        extractVars(t, policy.protectedDataVars, policy.intentBindingVars);
      }
    });


  }

  @Override
  public void visit(ElementPathBlock el) {

    el.getPattern().getList().forEach(t -> {
      if (insideIntentBinding) {
        extractVars(t.asTriple(), policy.intentBindingVars, policy.protectedDataVars);
      } else {
        extractVars(t.asTriple(), policy.protectedDataVars, policy.intentBindingVars);
      }
    });

  }

  @Override
  public void visit(ElementFilter el) {

  }

  @Override
  public void visit(ElementAssign el) {

  }

  @Override
  public void visit(ElementBind el) {

  }

  @Override
  public void visit(ElementData el) {

  }

  @Override
  public void visit(ElementUnion el) {
    el.getElements().forEach(e -> e.visit(this));
  }

  @Override
  public void visit(ElementOptional el) {
    el.getOptionalElement().visit(this);
  }

  @Override
  public void visit(ElementGroup el) {
    el.getElements().forEach(e -> e.visit(this));
  }

  @Override
  public void visit(ElementDataset el) {

  }

  @Override
  public void visit(ElementNamedGraph el) {
    Node graphNameNode = el.getGraphNameNode();
    if (graphNameNode.isConcrete() && graphNameNode.getURI().contains(IntentGraphHandler.INTENT_IRI)) {
      policy.intentBinding = el;
      this.insideIntentBinding = true;
    }
    el.getElement().visit(this);
    this.insideIntentBinding = false;
  }

  @Override
  public void visit(ElementExists el) {
    el.getElement().visit(this);
  }

  @Override
  public void visit(ElementNotExists el) {
    el.getElement().visit(this);
  }

  @Override
  public void visit(ElementMinus el) {
    el.getMinusElement().visit(this);
  }

  @Override
  public void visit(ElementService el) {
    el.getElement().visit(this);
  }

  @Override
  public void visit(ElementSubQuery el) {
    el.getQuery().getQueryPattern().visit(this);
  }

  private void extractVars(Triple t, Set<String> vars, Set<String> otherVars) {
    process(t.getSubject(), vars, otherVars);
    process(t.getPredicate(), vars, otherVars);
    process(t.getObject(), vars, otherVars);
  }

  private void process(Node node, Set<String> vars, Set<String> otherVars) {
    if (node.isVariable()) {
      vars.add(node.toString());
      if (otherVars.contains(node.toString())) {
        policy.minimalIntentBindingVars.add(node.toString());
      }
    }
  }
}
