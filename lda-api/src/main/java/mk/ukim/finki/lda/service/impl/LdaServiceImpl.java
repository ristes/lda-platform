package mk.ukim.finki.lda.service.impl;

import mk.ukim.finki.lda.service.DatasetProvider;
import mk.ukim.finki.lda.service.LdaService;
import mk.ukim.finki.lda.service.PolicyProvider;
import mk.ukim.finki.lda.service.PolicyTransformationService;
import mk.ukim.finki.lda.service.impl.processors.QueryProcessor;
import mk.ukim.finki.lda.service.impl.processors.UpdateDataDeleteProcessor;
import mk.ukim.finki.lda.service.impl.processors.UpdateDataInsertProcessor;
import mk.ukim.finki.lda.utils.LoadUtils;
import mk.ukim.finki.lda.web.dto.ConflictInfo;
import mk.ukim.finki.lda.web.dto.QueryResult;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.core.Quad;
import org.apache.jena.sparql.modify.request.QuadDataAcc;
import org.apache.jena.sparql.modify.request.UpdateDataDelete;
import org.apache.jena.sparql.modify.request.UpdateDataInsert;
import org.apache.jena.update.UpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Riste Stojanov
 */
@Service
public class LdaServiceImpl implements LdaService {


  @Autowired
  QueryProcessor processor;

  @Autowired
  PolicyProvider policyProvider;

  @Autowired
  DatasetProvider datasetProvider;

  @Autowired
  PolicyTransformationService transformationService;

  @Autowired
  UpdateDataInsertProcessor insertProcessor;


  @Autowired
  UpdateDataDeleteProcessor deleteProcessor;

  public static void makeResultFromQuery(Pair<String, QueryExecution> execPair, QueryResult result, Map prefixMapping) {

    List<Map<String, String>> solutions = new ArrayList<>();

    ResultSet res = execPair.getValue().execSelect();
    res.forEachRemaining(qs -> {
      Map<String, String> map = new HashMap<>();
      solutions.add(map);
      qs.varNames().forEachRemaining(vn -> map.put(vn, qs.get(vn).toString()));
    });


    result.query = execPair.getKey();
    result.variableNames = res.getResultVars();
    result.solutions = solutions;
    result.prefixMapping = prefixMapping;
  }

  public static QueryResult makeResult(Pair<String, List<Quad>> quadsPair, QueryResult result, Map prefixMapping) {

    List<Map<String, String>> solutions = new ArrayList<>();

    quadsPair.getValue().forEach(qs -> {
      Map<String, String> map = new HashMap<String, String>();
      solutions.add(map);
      map.put("g", qs.getGraph().toString());
      map.put("s", qs.getSubject().toString());
      map.put("p", qs.getPredicate().toString());
      map.put("o", qs.getObject().toString());
    });


    result.query = quadsPair.getKey();
    result.variableNames = Stream.of("g", "s", "p", "o").collect(Collectors.toList());
    result.solutions = solutions;
    result.prefixMapping = prefixMapping;
    return result;
  }

  @Override
  public QueryResult executeWithoutAuthorization(String sparqlQuery) {

    QueryResult result = new QueryResult();
    Query selectQuery = QueryFactory.create(sparqlQuery);
    QueryExecution exec = QueryExecutionFactory.create(selectQuery, datasetProvider.guardedDataset());
    datasetProvider.guardedDataset().begin(ReadWrite.READ);
    makeResultFromQuery(new ImmutablePair<>("", exec), result, selectQuery.getPrefixMapping().getNsPrefixMap());
    datasetProvider.guardedDataset().commit();
    return result;
  }

  @Override
  public QueryResult authorizedQuery(String query, Model intent) {
    Query sparqlQuery = QueryFactory.create(query);
    final QueryResult result = new QueryResult();
    processor.process(sparqlQuery, intent, p ->
      makeResultFromQuery(p, result, sparqlQuery.getPrefixMapping().getNsPrefixMap())
    );
    return result;
  }

  @Override
  public QueryResult coverage() throws IOException {

    String sparqlQuery = transformationService.coverageQuery(
      policyProvider.policies(),
      policyProvider.prefixes()
    );

    return executeWithoutAuthorization(sparqlQuery);
  }

  @Override
  public QueryResult unprotected() throws IOException {
    String sparqlQuery = transformationService.unprotectedQuery(
      policyProvider.policies(),
      policyProvider.prefixes()
    );

    return executeWithoutAuthorization(sparqlQuery);
  }

  @Override
  public List<ConflictInfo> detectConflicts(String name) throws IOException {

    List<ConflictInfo> results = transformationService.conflictInfoQueries(
      policyProvider.policyByName(name),
      policyProvider.policies(),
      policyProvider.prefixes()
    );

    List<ConflictInfo> conflicts = results.stream().map(ci -> {
      ci.result = executeWithoutAuthorization(ci.query);
      return ci;
    }).filter(ci -> ci.result.solutions.size() > 0).collect(Collectors.toList());

    return conflicts;
  }

  @Override
  public QueryResult insert(Iterator<Quad> quadIterator, Model intentModel) {
    QuadDataAcc acc = new QuadDataAcc();
    UpdateDataInsert insertQuery = new UpdateDataInsert(acc);
    quadIterator.forEachRemaining(acc::addQuad);
    MutablePair<String, List<Quad>> pair = new MutablePair<>();
    insertProcessor.process(new UpdateRequest(insertQuery), intentModel, c -> {
      pair.setLeft(c.getLeft());
      pair.setRight(c.getRight());
    });
    return makeResult(pair, new QueryResult(), LoadUtils.parsePrefixes(policyProvider.prefixes(), new HashMap()));
  }

  @Override
  public QueryResult delete(Iterator<Quad> quadIterator, Model intentModel) {
    QuadDataAcc acc = new QuadDataAcc();
    UpdateDataDelete deleteQuery = new UpdateDataDelete(acc);
    quadIterator.forEachRemaining(acc::addQuad);
    MutablePair<String, List<Quad>> pair = new MutablePair<>();
    deleteProcessor.process(new UpdateRequest(deleteQuery), intentModel, c -> {
      pair.setLeft(c.getLeft());
      pair.setRight(c.getRight());
    });
    return makeResult(pair, new QueryResult(), LoadUtils.parsePrefixes(policyProvider.prefixes(), new HashMap()));
  }
}
