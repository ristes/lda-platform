package mk.ukim.finki.lda.service;

import mk.ukim.finki.lda.model.Operation;
import mk.ukim.finki.lda.model.Policy;

import java.util.List;

/**
 * @author Riste Stojanov
 */
public interface PolicyProvider {

  Policy parse(String strPoilcy);

  List<Policy> policies();

  List<Policy> policies(Operation operation);

  Policy policyByName(String name);

  String prefixes();
}
