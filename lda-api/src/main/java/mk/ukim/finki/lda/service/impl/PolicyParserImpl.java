package mk.ukim.finki.lda.service.impl;

import mk.ukim.finki.lda.model.Operation;
import mk.ukim.finki.lda.model.Policy;
import mk.ukim.finki.lda.service.PolicyProvider;
import mk.ukim.finki.lda.service.impl.sparql.PolicyExtractionElementVisitor;
import mk.ukim.finki.lda.utils.LoadUtils;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.sparql.core.Quad;
import org.apache.jena.sparql.syntax.ElementGroup;
import org.apache.jena.sparql.syntax.ElementNamedGraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Riste Stojanov
 */
@Service
public class PolicyParserImpl implements PolicyProvider {

  private Environment environment;
  private List<Policy> policies = new ArrayList<>();

  @Autowired
  public PolicyParserImpl(Environment environment) {
    this.environment = environment;
  }

  @PostConstruct
  public void init() {
    try {
      System.out.println("************ lda.policiesPath" + environment.getProperty("lda.policiesPath"));
      Map<String, String> policySpecifications = LoadUtils.loadFiles(environment.getProperty("lda.policiesPath"));
      policySpecifications.forEach((key, value) -> {
        Policy p = parse(value);
        System.out.println("\t" + key);
        p.name = key.replaceAll(".rq", "");
        policies.add(p);
      });
    } catch (IOException e) {
      e.printStackTrace();
    }
    policies.sort(Comparator.comparingDouble(l -> l.priority));
    System.out.println("DONE");
  }

  @Override
  public Policy parse(String strPolicy) {
    try {
      Policy policy = new Policy();
      policy.query = strPolicy;

      strPolicy = strPolicy.replaceAll("\\{", " { ").replaceAll("\\}", " } ").replaceAll(" {2}", " ");


      int startIndex = strPolicy.toLowerCase().indexOf("allow");
      if (startIndex == -1) {
        startIndex = strPolicy.toLowerCase().indexOf("deny");
      }
      if (startIndex == -1) {
        throw new IllegalArgumentException("No permission (ALLOW/DENY) is present in the policy.");
      }

      String prefixes = strPolicy.substring(0, startIndex);
      policy.prefixes = prefixes;

      strPolicy = strPolicy.substring(startIndex);

      int whereIndex = strPolicy.toLowerCase().indexOf("where");


      String preamble = strPolicy.substring(0, whereIndex);
      String[] parts = preamble.split("[ \n]");

      if (parts[0].toLowerCase().equals("allow")) {
        policy.permission = true;
      } else if (parts[0].toLowerCase().equals("deny")) {
        policy.permission = false;
      } else {
        throw new IllegalArgumentException("Invalid permission: " + parts[0]);
      }

      policy.operation = Operation.valueOf(parts[1].toUpperCase());

      if (policy.operation != Operation.MANAGE) {
        policy.quad = extractQuad(parts);
      }

      int priorityIndex = strPolicy.toLowerCase().lastIndexOf("priority");
      int datasetIndex = strPolicy.toLowerCase().lastIndexOf("datasets");
      int endWhere;


      if (priorityIndex > 0) {
        String strPriority = strPolicy.substring(priorityIndex).split(" ")[1];
        policy.priority = Double.parseDouble(strPriority);
      }
      if (datasetIndex > 0) {
        String strDatasets = strPolicy.substring(datasetIndex).split(" ")[1];
        String[] datasets = strDatasets.split(",");
        Collections.addAll(policy.datasets, datasets);
      }

      String where;
      if (priorityIndex > 0 && datasetIndex > 0) {
        endWhere = Math.min(priorityIndex, datasetIndex);
        where = strPolicy.substring(whereIndex + 5, endWhere).trim();
      } else if ((priorityIndex > 0 || datasetIndex > 0)) {
        endWhere = Math.max(priorityIndex, datasetIndex);
        where = strPolicy.substring(whereIndex + 5, endWhere).trim();
      } else {
        where = strPolicy.substring(whereIndex + 5).trim();
      }
      String selQuery = prefixes + "SELECT * WHERE " + where;
      ElementGroup element = (ElementGroup) QueryFactory.create(selQuery)
        .getQueryPattern();

      element.visit(new PolicyExtractionElementVisitor(policy));

      element.getElements().remove(policy.intentBinding);
      policy.protectedData = element;

      policy.where = QueryFactory.create(selQuery).getQueryPattern();
      if (policy.intentBinding != null)
        policy.intentBinding = ((ElementNamedGraph) policy.intentBinding).getElement();


      return policy;
    } catch (RuntimeException ex) {
      System.err.println(strPolicy);
      throw ex;
    }
  }

  @Override
  public List<Policy> policies() {
    return policies;
  }

  @Override
  public List<Policy> policies(Operation operation) {
    return policies().stream().filter(p -> p.operation.matches(operation)).collect(Collectors.toList());
  }

  @Override
  public Policy policyByName(String name) {
    return policies().stream().filter(p -> p.name.equals(name)).findFirst().get();
  }

  @Override
  public String prefixes() {
    try {
      return LoadUtils.loadFile(environment.getProperty("lda.policyPrefixes"));
    } catch (IOException e) {
      e.printStackTrace();
      return "";
    }
  }

  private Quad extractQuad(String[] parts) {
    if (parts.length != 8) {
      throw new IllegalArgumentException("Invalid quad element structure!");
    }
    return new Quad(
      NodeFactory.createVariable(parts[6].trim().substring(1)),
      NodeFactory.createVariable(parts[3].trim().substring(1)),
      NodeFactory.createVariable(parts[4].trim().substring(1)),
      NodeFactory.createVariable(parts[5].trim().substring(1))
    );
  }
}
