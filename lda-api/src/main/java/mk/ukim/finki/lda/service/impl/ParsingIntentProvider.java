package mk.ukim.finki.lda.service.impl;

import mk.ukim.finki.lda.IntentProvider;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFParser;
import org.apache.jena.riot.system.ErrorHandlerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author Riste Stojanov
 */
@Service
public class ParsingIntentProvider implements IntentProvider<Pair<String, Lang>> {

  @Override
  public Model extractIntent(Pair<String, Lang> environment) {
    Model model = ModelFactory.createDefaultModel();
    try (InputStream in = new ByteArrayInputStream(environment.getLeft().getBytes())) {
      RDFParser.create()
        .source(in)
        .lang(environment.getRight())
        .errorHandler(ErrorHandlerFactory.errorHandlerStrict)
        .parse(model.getGraph());
    } catch (IOException e) {
      e.printStackTrace();
    }
    return model;
  }
}
