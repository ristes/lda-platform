package mk.ukim.finki.lda.service;

import mk.ukim.finki.lda.web.dto.ConflictInfo;
import mk.ukim.finki.lda.web.dto.QueryResult;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.core.Quad;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * @author Riste Stojanov
 */
public interface LdaService {

  QueryResult executeWithoutAuthorization(String sparqlQuery);

  QueryResult authorizedQuery(String query, Model intent);

  QueryResult coverage() throws IOException;


  QueryResult unprotected() throws IOException;

  List<ConflictInfo> detectConflicts(String name) throws IOException;

  QueryResult insert(Iterator<Quad> quadIterator, Model intentModel);

  QueryResult delete(Iterator<Quad> quadIterator, Model intentModel);
}
