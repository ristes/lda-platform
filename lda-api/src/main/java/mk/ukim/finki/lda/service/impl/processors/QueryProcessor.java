package mk.ukim.finki.lda.service.impl.processors;

import mk.ukim.finki.lda.model.Operation;
import mk.ukim.finki.lda.service.DatasetProvider;
import mk.ukim.finki.lda.service.PrologueProcessor;
import mk.ukim.finki.lda.service.TemporalDatasetProvider;
import mk.ukim.finki.lda.tds.IntentGraphHandler;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.core.DatasetGraph;
import org.apache.jena.sparql.core.Prologue;
import org.apache.jena.sparql.core.Quad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

/**
 * @author Riste Stojanov
 */
@Primary
@Service
public class QueryProcessor implements PrologueProcessor<QueryExecution> {

  @Autowired
  protected DatasetProvider datasetProvider;

  @Autowired
  TemporalDatasetProvider temporalDatasetProvider;

  Operation getOperation() {
    return Operation.READ;
  }

  @Override
  public void process(
    Prologue prologue,
    Model intent,
    Consumer<Pair<String, QueryExecution>> consumer) {

    if (!(prologue instanceof Query)) {
      throw new IllegalArgumentException();
    }
    Query query = (Query) prologue;

    Dataset dataset = datasetProvider.guardedDataset();
    String tmpDatasetQuery = temporalDatasetProvider.temporalDatasetQuery(getOperation());


    dataset.begin(ReadWrite.WRITE);
    // Register Intent
    String namedGraph = IntentGraphHandler.registerIntent(dataset, intent);
    tmpDatasetQuery = tmpDatasetQuery.replace(IntentGraphHandler.INTENT_IRI, namedGraph);

    dataset.commit();


    dataset.begin(ReadWrite.READ);
    System.out.println(tmpDatasetQuery);
    Dataset tmpDataset = getAllowedQuadsAsDataSet(dataset, tmpDatasetQuery);


    QueryExecution exec = QueryExecutionFactory.create(query, tmpDataset);

    consumer.accept(new ImmutablePair<>(tmpDatasetQuery, exec));

    dataset.commit();
    dataset.begin(ReadWrite.WRITE);
    dataset.removeNamedModel(namedGraph);
    dataset.commit();


  }

  private Dataset getAllowedQuadsAsDataSet(Dataset dataset, String tmpDatasetQuery) {

    Dataset result = datasetProvider.create();
    final DatasetGraph dsg = result.asDatasetGraph();
    try {
      try (QueryExecution qexec = QueryExecutionFactory.create(tmpDatasetQuery, dataset)) {

        ResultSet r = qexec.execSelect();
        r.forEachRemaining(qs -> {

          if (qs.get("?g") != null)
            dsg.add(
              qs.get("?g").asNode(),
              qs.get("?s").asNode(),
              qs.get("?p").asNode(),
              qs.get("?o").asNode());
          else
            dsg.add(
              Quad.defaultGraphIRI,
              qs.get("?s").asNode(),
              qs.get("?p").asNode(),
              qs.get("?o").asNode()
            );
        });
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return result;
  }
}
