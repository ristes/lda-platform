package mk.ukim.finki.lda.service;

import org.apache.jena.query.Dataset;

/**
 * @author Riste Stojanov
 */
public interface DatasetProvider {


  Dataset create();

  Dataset open(String path);

  Dataset guardedDataset();
}
