package mk.ukim.finki.lda.utils;

import mk.ukim.finki.lda.model.Policy;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.sparql.syntax.*;

import java.util.List;

/**
 * @author Riste Stojanov
 */
public class ElementUtils {

  public static ElementUnion createUnion(List<Policy> policies) {
    ElementUnion union = new ElementUnion();
    policies.forEach(policy -> union.addElement(policy.where));
    return union;
  }


  public static Element allowedDataElement(List<Policy> policies) {

    if (policies == null || policies.isEmpty()) {
      return new ElementGroup();
    }

    ElementUnion union = new ElementUnion();

    Policy first = policies.get(0);

    if (first.permission) {
      union.addElement(first.where);
    } else {
      union.addElement(createMinus(allDatasetSelector(), first.where));
    }

    for (int i = 1; i < policies.size(); i++) {
      Policy p = policies.get(i);
      if (p.permission) {
        union.addElement(p.where);
      } else {
        ElementUnion old = union;
        union = new ElementUnion();
        union.addElement(createMinus(old, p.where));
      }
    }
    return createGroup(union);

  }

  private static ElementGroup createGroup(Element element) {

    ElementGroup group = new ElementGroup();
    group.addElement(element);
    return group;
  }

  private static Element allDatasetSelector() {
    ElementTriplesBlock tb = new ElementTriplesBlock();
    tb.addTriple(new Triple(
      NodeFactory.createVariable("?s"),
      NodeFactory.createVariable("?p"),
      NodeFactory.createVariable("?o")
    ));
    return new ElementNamedGraph(NodeFactory.createVariable("?g"), tb);
  }

  private static ElementGroup createMinus(Element left, Element right) {

    ElementGroup group = createGroup(createGroup(left));
    ElementMinus minus = new ElementMinus(right);
    group.addElement(minus);
    return group;
  }

}
