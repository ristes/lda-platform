package mk.ukim.finki.lda.model;

/**
 * @author Riste Stojanov
 */
public enum Operation {
  READ, INSERT, DELETE, MODIFY {
    @Override
    public boolean matches(Operation other) {
      if (other.equals(INSERT) || other.equals(DELETE)) {
        return true;
      }
      return super.matches(other);
    }
  }, MANAGE, SIMULATE;

  public boolean matches(Operation other) {
    return this.equals(other);
  }
}
