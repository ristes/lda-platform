package mk.ukim.finki.lda.service;

import mk.ukim.finki.lda.model.Operation;
import mk.ukim.finki.lda.model.Policy;
import mk.ukim.finki.lda.web.dto.ConflictInfo;

import java.util.List;

/**
 * @author Riste Stojanov
 */
public interface PolicyTransformationService {

  String coverageQuery(List<Policy> policies, String prefixes);

  String unprotectedQuery(List<Policy> policies, String prefixes);

  List<ConflictInfo> conflictInfoQueries(Policy policy, List<Policy> policies, String prefixes);

  String allowedDataQuery(List<Policy> policies, Operation operation);
}
