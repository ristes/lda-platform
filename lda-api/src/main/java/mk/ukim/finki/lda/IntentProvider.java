package mk.ukim.finki.lda;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;

/**
 * @author Riste Stojanov
 */
public interface IntentProvider<T> {


  Model extractIntent(T environment);
}
