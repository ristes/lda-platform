package mk.ukim.finki.lda.service.impl.processors;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.core.DatasetGraph;
import org.apache.jena.sparql.core.Quad;
import org.apache.jena.sparql.modify.request.UpdateDataDelete;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author Riste Stojanov
 */
@Service
public class UpdateDataDeleteProcessor extends AbstractUpdateProcessor<UpdateDataDelete, List<Quad>> {


  public void doProcess(
    UpdateDataDelete update,
    Model intent,
    Dataset dataset,
    String query,
    Consumer<Pair<String, List<Quad>>> consumer) {

    // Insert the quads
    List<Quad> quads = update.getQuads();

    DatasetGraph dsg = dataset.asDatasetGraph();

    List<Quad> deleted = new ArrayList<>();

    // TODO: optimization with filter exists and values
    consumeAllowed(dataset, query,
      aq -> {
        if (quads.contains(aq)) {
          dsg.delete(aq);
          deleted.add(aq);
        }
      });

    consumer.accept(new ImmutablePair<>(query, deleted));
  }
}
