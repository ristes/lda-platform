package mk.ukim.finki.lda.service.impl.processors;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.core.Quad;
import org.apache.jena.sparql.modify.request.UpdateDataInsert;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author Riste Stojanov
 */

@Service
public class UpdateDataInsertProcessor extends AbstractUpdateProcessor<UpdateDataInsert, List<Quad>> {

  public void doProcess(
    UpdateDataInsert update,
    Model intent,
    Dataset dataset,
    String query,
    Consumer<Pair<String, List<Quad>>> consumer) {

    // Insert the requestedInsertQuads
    List<Quad> requestedInsertQuads = new ArrayList<>(update.getQuads());

    List<Quad> inserted = new ArrayList<>();

    requestedInsertQuads.forEach(q -> dataset.asDatasetGraph().add(q));

    // remove the allowed quads form the requested ones
    consumeAllowed(dataset, query,
      allowedQuad -> {
        if (requestedInsertQuads.remove(allowedQuad)) {
          inserted.add(allowedQuad);
        }
      });

    // remove the forbidden requestedInsertQuads
    requestedInsertQuads.forEach(q -> dataset.asDatasetGraph().delete(q));


    consumer.accept(new ImmutablePair<>(query, inserted));

  }
}
