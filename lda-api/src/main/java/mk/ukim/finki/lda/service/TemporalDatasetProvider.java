package mk.ukim.finki.lda.service;

import mk.ukim.finki.lda.model.Operation;

/**
 * @author Riste Stojanov
 */
public interface TemporalDatasetProvider {

  String temporalDatasetQuery(Operation operation);
}
