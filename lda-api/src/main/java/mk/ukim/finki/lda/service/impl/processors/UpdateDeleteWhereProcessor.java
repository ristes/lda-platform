package mk.ukim.finki.lda.service.impl.processors;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.core.DatasetGraph;
import org.apache.jena.sparql.core.Quad;
import org.apache.jena.sparql.modify.request.UpdateDeleteWhere;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author Riste Stojanov
 */
public class UpdateDeleteWhereProcessor extends AbstractUpdateProcessor<UpdateDeleteWhere, List<Quad>> {

  public void doProcess(
    UpdateDeleteWhere update,
    Model intent,
    Dataset dataset,
    String query,
    Consumer<Pair<String, List<Quad>>> consumer) {

    // Insert the quads
    List<Quad> quads = update.getQuads();

    List<Quad> deleted = new ArrayList<>();

    DatasetGraph dsg = dataset.asDatasetGraph();

    consumeAllowed(dataset, query,
      aq -> {
        for (Quad qp : quads) {
          if (qp.matches(aq.getGraph(), aq.getSubject(), aq.getPredicate(), aq.getObject())) {
            dsg.delete(aq);
            deleted.add(aq);
          }
        }

      });

    consumer.accept(new ImmutablePair<>(query, deleted));
  }
}
