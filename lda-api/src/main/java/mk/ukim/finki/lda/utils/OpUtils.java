package mk.ukim.finki.lda.utils;

import org.apache.jena.sparql.algebra.Op;
import org.apache.jena.sparql.algebra.op.OpUnion;

import java.util.List;

/**
 * @author Riste Stojanov
 */
public class OpUtils {

  public static Op createUnion(List<Op> allow) {
    if (allow.isEmpty()) {
      return null;
    }
    if (allow.size() == 1) {
      return allow.get(0);
    }
    Op union = OpUnion.create(allow.get(0), allow.get(1));
    return allow.stream().skip(2).reduce(OpUnion::create).orElse(union);
  }
}
