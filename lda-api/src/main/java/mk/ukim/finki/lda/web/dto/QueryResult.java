package mk.ukim.finki.lda.web.dto;

import java.util.List;
import java.util.Map;

/**
 * @author Riste Stojanov
 */
public class QueryResult {


  public List<String> variableNames;
  public List<Map<String, String>> solutions;
  public Map<String, String> prefixMapping;
  public String query;
}
