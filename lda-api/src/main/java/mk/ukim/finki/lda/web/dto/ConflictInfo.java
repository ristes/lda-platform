package mk.ukim.finki.lda.web.dto;

import mk.ukim.finki.lda.model.Policy;

/**
 * @author Riste Stojanov
 */
public class ConflictInfo {

  public ConflictInfo(Policy original, Policy conflict, String query) {
    this.original = original;
    this.other = conflict;
    this.query = query;
  }

  public Policy original;

  public Policy other;

  public String query;

  public QueryResult result;
}
