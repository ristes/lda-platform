package mk.ukim.finki.lda.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.jena.sparql.core.Quad;
import org.apache.jena.sparql.syntax.Element;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Riste Stojanov
 */
public class Policy {
  public String name;

  public boolean permission;

  public Operation operation;

  @JsonSerialize(using = ElementConverter.class)
  public Element where;

  @JsonSerialize(using = ElementConverter.class)
  public Element intentBinding;

  @JsonSerialize(using = ElementConverter.class)
  public Element protectedData;


  public double priority;

  public List<String> datasets = new ArrayList<>();


  @JsonSerialize(using = QuadSerializer.class)
  public Quad quad;


  public String prefixes;

  public Set<String> intentBindingVars = new HashSet<>();

  public Set<String> protectedDataVars = new HashSet<>();

  public Set<String> minimalIntentBindingVars = new HashSet<>();

  public String query;
}
