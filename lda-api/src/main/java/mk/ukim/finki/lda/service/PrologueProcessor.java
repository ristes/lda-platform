package mk.ukim.finki.lda.service;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.core.Prologue;

import java.util.function.Consumer;

/**
 * @author Riste Stojanov
 */
public interface PrologueProcessor<R> {

  void process(
    Prologue prologue,
    Model intent,
    Consumer<Pair<String, R>> consumer);
}
