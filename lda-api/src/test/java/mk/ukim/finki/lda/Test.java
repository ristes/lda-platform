package mk.ukim.finki.lda;

import mk.ukim.finki.lda.utils.LoadUtils;
import mk.ukim.finki.lda.web.SecuredEndpoint;
import mk.ukim.finki.lda.web.dto.QueryResult;
import org.apache.commons.io.IOUtils;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.riot.RDFLanguages;
import org.apache.jena.sparql.core.DatasetGraph;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * @author Riste Stojanov
 */
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class Test {

  @Autowired
  Environment environment;

  @Autowired
  SecuredEndpoint endpoint;

  @org.junit.Test
  public void main() throws IOException {

    DatasetGraph dsg = LoadUtils.parseDataGraph(
      DatasetFactory.create().asDatasetGraph(),
      this.getClass().getResourceAsStream("/observations.nq"),
      RDFLanguages.NQUADS
    );

    String intent = IOUtils.toString(this.getClass().getResourceAsStream("/intents/john.jsonld"), Charset.defaultCharset());
    String data = IOUtils.toString(this.getClass().getResourceAsStream("/in/bob-new-observation.trig"), Charset.defaultCharset());


    QueryResult result = endpoint.insert(data, intent);

    System.out.println(result.query);
    System.out.println("==========================");
    result.solutions.forEach(System.out::println);
  }

}
