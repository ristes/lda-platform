package mk.ukim.finki.lda;

import mk.ukim.finki.lda.service.DatasetProvider;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.tdb.TDB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 * @author Riste Stojanov
 */
@Primary
@Service
public class TestDatasetProviderImpl implements DatasetProvider {

  @Autowired
  Environment environment;

  private Dataset guardedDataset;

  public Dataset create() {
    Dataset ds = DatasetFactory.create();
    ds.asDatasetGraph().getContext().set(TDB.symUnionDefaultGraph, true);
    return ds;
  }

  public Dataset open(String path) {
    return guardedDataset();
  }


  @Override
  public Dataset guardedDataset() {
    if (guardedDataset == null) {
      guardedDataset = create();
    }
    return guardedDataset;
  }
}
