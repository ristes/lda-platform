import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public data = `@prefix ex:    &lt;http://example.com/&gt; .
@prefix rdf:   &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt; .
@prefix xsd:   &lt;http://www.w3.org/2001/XMLSchema#&gt; .
@prefix sm:    &lt;http://sm.example.com#&gt; .
@prefix sp:    &lt;http://spin.com#&gt; .
@prefix int:   &lt;http://int.example.com#&gt; .

ex:hospital {
    ex:alice  a                 sm:User ;
            sm:emergency_phone  "075 987 654" ;
            sm:uses             ex:ssa .

    ex:hospital  a              sm:Hospital ;
            sm:location         [ sm:latitude   "42.004"^^xsd:double ;
                                  sm:longitude  "21.409"^^xsd:double
                                ] ;
            sm:network_address  "192.168.100.0/24" .

    ex:john  a           sm:User ;
            sm:phone     "070 111 111" ;
            sm:works_at  ex:hospital .

    ex:t1   a               sm:Treatment ;
            sm:for_patient  ex:bob ;
            sm:from         "2017-07-20" ;
            sm:has_doctor   ex:john ;
            sm:to           "2017-09-20" .

    ex:ssa  a               sm:SensorSyncApplicaton ;
            sm:provided_by  ex:hospital .

    ex:bob  a                   sm:User ;
            sm:emergency_phone  "075 123 456" ;
            sm:uses             ex:ssa .

    ex:ben  a            sm:User ;
            sm:phone     "075 555 555" ;
            sm:works_at  ex:hospital .

    ex:t2   a               sm:Treatment ;
            sm:for_patient  ex:alice ;
            sm:from         "2017-04-13" ;
            sm:has_doctor   ex:ben ;
            sm:to           "2017-04-23" .

    ex:s1   a                sm:HealthSensor ;
            sm:owner         ex:bob ;
            sm:regular_from  "60"^^xsd:int ;
            sm:regular_to    "140"^^xsd:int ;
            sm:stype         "Pulse" ;
            sm:unit          "bpm" .

    ex:t3   a               sm:Treatment ;
            sm:for_patient  ex:john ;
            sm:from         "2017-07-01" ;
            sm:has_doctor   ex:ben ;
            sm:to           "2017-09-20" .

    ex:s2   a            sm:Sensor ;
            sm:location  [ sm:latitude   "42.010"^^xsd:double ;
                           sm:longitude  "21.410"^^xsd:double
                         ] ;
            sm:owner     ex:alice ;
            sm:stype     "Temperature" ;
            sm:unit      "C" .
}

ex:ssa {
    ex:o1   a          sm:Observation ;
            sm:sensor  ex:s1 ;
            sm:time    "1500386600319"^^xsd:long ;
            sm:val     "66"^^xsd:int .

    ex:o3   a          sm:Observation ;
            sm:sensor  ex:s2 ;
            sm:time    "1500386690319"^^xsd:long ;
            sm:val     "28"^^xsd:int .

    ex:o2   a          sm:Observation ;
            sm:sensor  ex:s1 ;
            sm:time    "1500386690319"^^xsd:long ;
            sm:val     "57"^^xsd:int .
}`;

  constructor() {
  }

  ngOnInit() {
  }

}
