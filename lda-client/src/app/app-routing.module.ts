import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ShowcaseComponent} from './showcase/showcase.component';
import {ManageComponent} from './manage/manage.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {PolicyComponent} from './manage/policy/policy.component';
import {ModifyComponent} from './modify/modify.component';

const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'query', component: ShowcaseComponent},
  {path: 'modify', component: ModifyComponent},
  {path: 'manage', component: ManageComponent},
  {path: 'manage/policy/:name', component: PolicyComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
