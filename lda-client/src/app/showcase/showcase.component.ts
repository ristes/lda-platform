import {AfterViewInit, Component, OnInit} from '@angular/core';
import {QuerySolution} from '../model/query-solution';
import {Constants} from '../services/constants';
import {QueryService} from '../services/query/query.service';

@Component({
  selector: 'app-showcase',
  templateUrl: './showcase.component.html',
  styleUrls: ['./showcase.component.css']
})
export class ShowcaseComponent implements AfterViewInit, OnInit {

  public querySolution: QuerySolution;
  public context = false;
  public displayCfg = Constants.sparqlCodeMirrorCfg;

  query = Constants.selectQuery;
  intent = Constants.bobsIntent;

  constructor(private queryService: QueryService) {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {

  }


  execQuery(): void {
    this.queryService.execQuery(this.query, this.intent).then(qs => this.querySolution = qs);
  }


}
