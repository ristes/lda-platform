import {Policy} from './policy';
import {QuerySolution} from './query-solution';

export class ConflictInfo {

  public original: Policy;
  public other: Policy;
  public query: string;
  public result: QuerySolution;

}
