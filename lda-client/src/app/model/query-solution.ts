export class QuerySolution {

  public prefixMapping: Map<string, string>;
  public variableNames: string[];
  public solutions: Map<string, string>[];
  public query: string;
}
