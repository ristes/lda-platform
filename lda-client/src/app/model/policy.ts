export class Policy {

  public name: string;

  public permission: boolean;

  public operation: string;

  public intentBinding: string;

  public protectedData: string;

  public priority: number;

  public intentBindingVars: Array<string>;

  public minimalIntentBindingVars: Array<string>;

  public protectedDataVars: Array<string>;

  public prefixes: string;

  public quad: string;

  public query: string;

}
