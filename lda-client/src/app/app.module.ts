import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {CodemirrorModule} from 'ng2-codemirror';
import {HttpModule} from '@angular/http';
import {ShowcaseComponent} from './showcase/showcase.component';
import {ManageComponent} from './manage/manage.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AppRoutingModule} from './app-routing.module';
import {QuerySolutionComponent} from './query-solution/query-solution.component';
import {PolicyComponent} from './manage/policy/policy.component';
import {PolicyPreviewComponent} from './manage/policy-preview/policy-preview.component';
import {ModifyComponent} from './modify/modify.component';
import {QueryService} from './services/query/query.service';
import {ManageService} from './services/manage/manage.service';


@NgModule({
  declarations: [
    AppComponent,
    ShowcaseComponent,
    ManageComponent,
    DashboardComponent,
    QuerySolutionComponent,
    PolicyComponent,
    PolicyPreviewComponent,
    ModifyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CodemirrorModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [QueryService, ManageService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
