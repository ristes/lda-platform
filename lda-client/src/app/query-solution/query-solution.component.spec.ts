import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuerySolutionComponent } from './query-solution.component';

describe('QuerySolutionComponent', () => {
  let component: QuerySolutionComponent;
  let fixture: ComponentFixture<QuerySolutionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuerySolutionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuerySolutionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
