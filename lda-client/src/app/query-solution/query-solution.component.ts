import {Component, Input, OnInit} from '@angular/core';
import {QuerySolution} from '../model/query-solution';
import {Constants} from '../services/constants';

@Component({
  selector: 'query-solution',
  templateUrl: './query-solution.component.html',
  styleUrls: ['./query-solution.component.css']
})
export class QuerySolutionComponent implements OnInit {

  private _querySolution: QuerySolution;
  public showQuery = false;

  public displayCfg = Constants.sparqlCodeMirrorCfg;

  @Input()
  set querySolution(value: QuerySolution) {
    this._querySolution = value;
    this.onQuerySolution();
  }

  get querySolution(): QuerySolution {
    return this._querySolution;
  }

  constructor() {
  }

  ngOnInit() {

  }

  private onQuerySolution() {
    this.querySolution.solutions.forEach(solution => {
      this.querySolution.variableNames.forEach(varName => {
        for (const p in this.querySolution.prefixMapping) {
          if (this.querySolution.prefixMapping.hasOwnProperty(p) && solution[varName]) {
            solution[varName] = solution[varName].replace(this.querySolution.prefixMapping[p], p + ':');
          }
        }
      });
    });
  }

}
