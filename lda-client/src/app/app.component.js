"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("codemirror/mode/sparql/sparql");
require("codemirror/mode/turtle/turtle");
require("codemirror/mode/javascript/javascript");
var AppComponent = (function () {
    function AppComponent(queryService) {
        var _this = this;
        this.queryService = queryService;
        this.requester = 'ex:alice';
        this.callback = function (result) {
            result.solutions.forEach(function (s) {
                result.variableNames.forEach(function (v) {
                    for (var p in result.prefixMapping) {
                        if (result.prefixMapping.hasOwnProperty(p) && s[v]) {
                            s[v] = s[v].replace(result.prefixMapping[p], p + ':');
                        }
                    }
                });
            });
            _this.solutions = result.solutions;
            _this.varNames = result.variableNames;
        };
        this.title = 'app';
        this.displayCfg = {
            lineNumbers: true,
            mode: "application/sparql-query",
            matchBrackets: true
        };
        this.intentCfg = {
            lineNumbers: true,
            mode: "text/turtle",
            matchBrackets: true
        };
        this.query = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nPREFIX ex: <http://example.com/>\nPREFIX sm: <http://sm.example.com#>\nPREFIX int: <http://int.example.com#>\nPREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\nPREFIX sp: <http://spin.com#>\n\nSELECT *\nWHERE {\n    GRAPH ?g {\n      ?s rdf:type sm:Treatment. \n      ?s ?p ?o\n    }\n}";
        this.actualQuery = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\nPREFIX ex: <http://example.com/>\nPREFIX sm: <http://sm.example.com#>\nPREFIX int: <http://int.example.com#>\nPREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\nPREFIX sp: <http://spin.com#>\n\nSELECT ?s ?p ?o ?g \nWHERE {\n  Graph ?g {\n   ?s ?p ?o.\n  }\n  ?s rdf:type sm:Treatment\n\tbind(" + this.requester + " as ?r).\n\t{\n\t\t{\n\t\t\t?r rdf:type sm:User.\n\t\t\t?r ?v6 ?s\n\t\t}\n\t\tUNION\n\t\t{\n\t\t\t?r rdf:type sm:User.\n\t\t\t?s ?v7 ?r\n\t\t}\n\t\tUNION\n\t\t{\t\n\t\t\t?r rdf:type sm:User.\n\t\t\tBIND(?r as ?s)\n\t\t}\n\t}\n} ";
        this.intent = "{\n    \"@context\": {\n      \"int\": \"http://int.example.com#\",\n      \"sp\": \"http://spin.com#\",\n      \"ex\": \"http://example.com/\"\n    },\n    \"@graph\": [\n      {\n        \"int:action\": {\n          \"@type\": \"sp:Select\"\n        },\n        \"int:requester\": {\n          \"@id\": \"ex:john\",\n          \"@type\": \"int:Requester\"\n        },\n        \"int:agent\": {\n          \"@type\": \"int:Agent\",\n          \"int:user_agent\": \"Mozila 5.0...\",\n          \"int:address\": {\n            \"int:ip_value\": \"192.168.100.1\",\n            \"int:ip_network\": \"192.168.100.0/24\"\n          }\n        }\n      }\n    ]\n  }";
    }
    AppComponent.prototype.ngAfterViewInit = function () {
    };
    AppComponent.prototype.execQuery = function () {
        this.queryService.execQuery(this.query, this.intent).then(this.callback);
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'app-root',
        templateUrl: './app.component.html',
        styleUrls: ['./app.component.css']
    })
], AppComponent);
exports.AppComponent = AppComponent;
