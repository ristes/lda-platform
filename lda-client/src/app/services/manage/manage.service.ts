import {Injectable} from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import {Policy} from '../../model/policy';
import {QuerySolution} from '../../model/query-solution';
import {ConflictInfo} from '../../model/conflict-info';

@Injectable()
export class ManageService {

  private policiesUrl = 'http://api.lda.finki.ukim.mk/policies';

  private coverageUrl = 'http://api.lda.finki.ukim.mk/coverage';
  private unprotectedUrl = 'http://api.lda.finki.ukim.mk/unprotected';

  private parseUrl = 'http://api.lda.finki.ukim.mk/parse';
  private simulateUrl = 'http://api.lda.finki.ukim.mk/simulate';
  private conflictUrl = 'http://api.lda.finki.ukim.mk/conflict';

  private static handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }


  constructor(private http: Http) {
  }

  public policies(): Promise<Array<Policy>> {
    return this.http.get(this.policiesUrl)
      .toPromise()
      .then(response => response.json() as Array<Policy>)
      .catch(ManageService.handleError);
  }

  public coverage(): Promise<QuerySolution> {
    return this.http.get(this.coverageUrl)
      .toPromise()
      .then(response => response.json() as QuerySolution)
      .catch(ManageService.handleError);
  }

  public unprotected(): Promise<QuerySolution> {
    return this.http.get(this.unprotectedUrl)
      .toPromise()
      .then(response => response.json() as QuerySolution)
      .catch(ManageService.handleError);
  }

  public policy(name: string): Promise<Policy> {
    return this.http.get(this.policiesUrl)
      .toPromise()
      .then(response => response.json() as Array<Policy>)
      .then(policies => {
        let result: Policy = null;
        policies.forEach(p => {
            if (p.name === name) {
              result = p;
            }
          }
        );
        return result;
      })
      .catch(ManageService.handleError);
  }


  public parse(query: string): Promise<Policy> {
    const params = new URLSearchParams();
    params.set('query', query);
    return this.http.get(this.parseUrl, {params: params})
      .toPromise()
      .then(response => response.json() as Policy)
      .catch(ManageService.handleError);
  }


  public simulate(query: string): Promise<QuerySolution> {
    const params = new URLSearchParams();
    params.set('query', query);
    return this.http.get(this.simulateUrl, {params: params})
      .toPromise()
      .then(response => response.json() as QuerySolution)
      .catch(ManageService.handleError);
  }

  public conflict(name: string): Promise<Array<ConflictInfo>> {
    const params = new URLSearchParams();
    params.set('name', name);
    return this.http.get(this.conflictUrl, {params: params})
      .toPromise()
      .then(response => response.json() as Array<ConflictInfo>)
      .catch(ManageService.handleError);
  }


}
