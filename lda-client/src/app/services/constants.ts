export class Constants {

  public static selectQuery = `PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX ex: <http://example.com/>
PREFIX sm: <http://sm.example.com#>
PREFIX int: <http://int.example.com#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX sp: <http://spin.com#>

SELECT *
WHERE {
    GRAPH ?g {
      ?s rdf:type sm:Observation.
      ?s ?p ?o
    }
}`;

  public static insertQuery = `@prefix ex:    <http://example.com/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .
@prefix sm:    <http://sm.example.com#> .
@prefix sp:    <http://spin.com#> .
@prefix int:   <http://int.example.com#> .

ex:ssa {
    ex:o5   a          sm:Observation ;
            sm:sensor  ex:s1 ;
            sm:time    "1500386600319"^^xsd:integer;
            sm:val     "66"^^xsd:integer .
}`;

  public static bobsIntent = `{
    "@context": {
      "int": "http://int.example.com#",
      "sp": "http://spin.com#",
      "ex": "http://example.com/"
    },
    "@graph": [
      {
        "int:action": {
          "@type": "sp:Select"
        },
        "int:requester": {
          "@id": "ex:bob",
          "@type": "int:Requester"
        },
        "int:agent": {
          "@type": "int:Agent",
          "int:user_agent": "Mozila 5.0...",
          "int:address": {
            "int:ip_value": "192.168.100.1",
            "int:network": "192.168.100.0/24"
          }
        }
      }
    ]
  }`;

  public static johnsIntent = `{
    "@context": {
      "int": "http://int.example.com#",
      "sp": "http://spin.com#",
      "ex": "http://example.com/"
    },
    "@graph": [
      {
        "int:action": {
          "@type": "sp:Select"
        },
        "int:requester": {
          "@id": "ex:john",
          "@type": "int:Requester"
        },
        "int:agent": {
          "@type": "int:Agent",
          "int:user_agent": "Mozila 5.0...",
          "int:address": {
            "int:ip_value": "192.168.100.1",
            "int:network": "192.168.100.0/24"
          }
        }
      }
    ]
  }`;

  public static sparqlCodeMirrorCfg = {
    lineNumbers: true,
    mode: 'application/sparql-query',
    matchBrackets: true
  };
}
