import {Injectable} from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {QuerySolution} from '../../model/query-solution';


@Injectable()
export class QueryService {

  private url = 'http://api.lda.finki.ukim.mk/sparql';
  private insertUrl = 'http://api.lda.finki.ukim.mk/insert';
  private removeUrl = 'http://api.lda.finki.ukim.mk/delete';

  private static handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  constructor(private http: Http) {
  }

  public execQuery(query: string, intent: string): Promise<QuerySolution> {
    const params = new URLSearchParams();
    params.set('query', query);
    params.set('intent', intent);
    return this.http.get(this.url, {params: params})
      .toPromise()
      .then(response => response.json() as QuerySolution)
      .catch(QueryService.handleError);
  }


  public insert(query: string, intent: string): Promise<QuerySolution> {
    const params = new URLSearchParams();
    params.set('data', query);
    params.set('intent', intent);
    return this.http.get(this.insertUrl, {params: params})
      .toPromise()
      .then(response => response.json() as QuerySolution)
      .catch(QueryService.handleError);
  }


  public remove(query: string, intent: string): Promise<QuerySolution> {
    const params = new URLSearchParams();
    params.set('data', query);
    params.set('intent', intent);
    return this.http.get(this.removeUrl, {params: params})
      .toPromise()
      .then(response => response.json() as QuerySolution)
      .catch(QueryService.handleError);
  }


}
