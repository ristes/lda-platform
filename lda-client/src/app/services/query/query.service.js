"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var QueryService = (function () {
    function QueryService(http) {
        this.http = http;
        this.url = 'http://localhost:8080/sparql';
    }
    QueryService.prototype.execQuery = function (query, intent) {
        var params = new http_1.URLSearchParams();
        params.set('query', query);
        params.set('intent', intent);
        return this.http.get(this.url, { params: params })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    QueryService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    return QueryService;
}());
QueryService = __decorate([
    core_1.Injectable()
], QueryService);
exports.QueryService = QueryService;
