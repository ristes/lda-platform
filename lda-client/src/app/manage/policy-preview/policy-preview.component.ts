import {Component, Input, OnInit} from '@angular/core';
import {Policy} from '../../model/policy';

@Component({
  selector: 'policy-preview',
  templateUrl: './policy-preview.component.html',
  styleUrls: ['./policy-preview.component.css']
})
export class PolicyPreviewComponent implements OnInit {

  @Input() policy: Policy;


  constructor() {
  }

  ngOnInit() {
  }

}
