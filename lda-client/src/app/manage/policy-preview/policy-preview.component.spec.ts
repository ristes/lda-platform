import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolicyPreviewComponent } from './policy-preview.component';

describe('PolicyPreviewComponent', () => {
  let component: PolicyPreviewComponent;
  let fixture: ComponentFixture<PolicyPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolicyPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
