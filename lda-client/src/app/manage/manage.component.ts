import {Component, OnInit} from '@angular/core';
import {Policy} from '../model/policy';
import {QuerySolution} from '../model/query-solution';
import {ManageService} from '../services/manage/manage.service';

@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class ManageComponent implements OnInit {
  policies: Policy[];
  public querySolution: QuerySolution;
  public resultsTitle: string;


  constructor(private manageService: ManageService) {
  }

  ngOnInit() {
    this.manageService.policies().then(policies => this.policies = policies);
  }

  public overallCoverage() {
    this.resultsTitle = 'Coverage ';
    this.manageService.coverage().then(qs => this.querySolution = qs);

  }

  public unprotectedQuads() {

    this.resultsTitle = 'Unprotected ';
    this.manageService.unprotected().then(qs => this.querySolution = qs);
  }
}
