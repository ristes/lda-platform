import {Component, OnInit} from '@angular/core';
import {Policy} from '../../model/policy';
import {QuerySolution} from '../../model/query-solution';
import {ActivatedRoute, ParamMap} from '@angular/router';

import 'rxjs/add/operator/switchMap';
import {ConflictInfo} from '../../model/conflict-info';
import {ManageService} from '../../services/manage/manage.service';

@Component({
  selector: 'manage-policy',
  templateUrl: './policy.component.html',
  styleUrls: ['./policy.component.css']
})
export class PolicyComponent implements OnInit {


  query: string;
  displayCfg: any;
  querySolution: QuerySolution;
  intent: Object = {
    '?r': 'ex:john',
    '?ag': '_:b0',
    '?ip': '_:b1',
    '?n': '"192.168.100.0/24"'
  };
  policy: Policy;
  resultsTitle: string;
  conflicts: Array<ConflictInfo>;
  private policyName: string;


  constructor(private manageService: ManageService,
              private route: ActivatedRoute) {
    this.initFields();
  }

  ngOnInit() {

    this.route.paramMap
      .switchMap((params: ParamMap) => {
        this.policyName = params.get('name');
        return this.manageService.policy(this.policyName);
      }).subscribe(policy => {
      this.policy = policy;
      this.query = policy.query;

      this.conflicts = null;
      this.querySolution = null;
    });
  }

  public parse() {
    this.manageService.parse(this.query).then(policy => {
      this.policy = policy;
      this.policy.name = this.policyName;
    });
  }


  public execute() {
    this.resultsTitle = 'Simulation Results';

    this.manageService.simulate(this.makeQuery())
      .then(qs => this.querySolution = qs);

  }

  public checkConflict() {
    this.manageService.conflict(this.policy.name)
      .then(conflicts => {
        this.conflicts = conflicts;
        this.querySolution = null;
      });
  }

  public save() {

  }

  public coverage() {

    this.resultsTitle = 'Policy Coverage';
    let intentBinding: string = this.policy.prefixes;
    const where: string = this.policy.protectedData;
    intentBinding += ' SELECT ';
    intentBinding += this.policy.quad ? this.policy.quad.substr(1, this.policy.quad.length - 2) : ' * ';

    intentBinding += ' WHERE ';

    intentBinding += where;
    console.log(intentBinding);
    this.manageService.simulate(intentBinding)
      .then(qs => this.querySolution = qs);
  }

  public coveragePerIntent() {
    this.resultsTitle = 'Coverage per Intent';
    let intentBinding: string = this.policy.prefixes;
    let where: string = this.policy.protectedData;
    intentBinding += ' SELECT ';
    intentBinding += this.policy.quad ? this.policy.quad.substr(1, this.policy.quad.length - 2) : ' * ';


    if (this.policy.minimalIntentBindingVars.length > 0) {
      where += ' ORDER BY ';
    }
    for (let i = 0; i < this.policy.minimalIntentBindingVars.length; i++) {
      const k = this.policy.minimalIntentBindingVars[i];
      if (this.intent.hasOwnProperty(k)) {
        intentBinding += ' ' + k + ' ';
        where += ' ' + k + ' ';
      }
    }

    intentBinding += ' WHERE ';
    intentBinding += where;
    console.log(intentBinding);
    this.manageService.simulate(intentBinding)
      .then(qs => this.querySolution = qs);
  }

  private makeQuery(): string {

    let intentBinding: string = this.policy.prefixes;
    let where: string = this.policy.protectedData;
    intentBinding += ' SELECT ';
    intentBinding += this.policy.quad ? this.policy.quad.substr(1, this.policy.quad.length - 2) : ' * ';

    intentBinding += ' WHERE ';

    for (const k in  this.intent) {
      if (this.intent.hasOwnProperty(k)) {
        where = where.replace(new RegExp(k.replace('?', '\\?'), 'g'), this.intent[k]);
      }
    }
    intentBinding += where;
    console.log(intentBinding);
    return intentBinding;
  }

  private initFields() {

    this.displayCfg = {
      lineNumbers: true,
      mode: 'application/sparql-query',
      matchBrackets: true
    };

  }
}
