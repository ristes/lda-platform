import {Component, OnInit} from '@angular/core';
import {QueryService} from '../services/query/query.service';
import {Constants} from '../services/constants';
import {QuerySolution} from '../model/query-solution';

@Component({
  selector: 'app-modify',
  templateUrl: './modify.component.html',
  styleUrls: ['./modify.component.css']
})
export class ModifyComponent implements OnInit {


  public querySolution: QuerySolution;
  public context = false;
  public displayCfg = Constants.sparqlCodeMirrorCfg;

  query = Constants.insertQuery;
  intent = Constants.johnsIntent;

  constructor(private queryService: QueryService) {
  }

  ngOnInit() {
  }

  insert(): void {
    this.queryService.insert(this.query, this.intent).then(qs => this.querySolution = qs);
  }

  remove(): void {
    this.queryService.remove(this.query, this.intent).then(qs => this.querySolution = qs);
  }

}
